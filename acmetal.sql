-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2+deb7u1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Czas wygenerowania: 11 Cze 2019, 12:33
-- Wersja serwera: 5.5.41
-- Wersja PHP: 5.4.45-0+deb7u8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Baza danych: `c7acmetal`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ContactFormMessage`
--

CREATE TABLE IF NOT EXISTS `ContactFormMessage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `senderName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `senderEmail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `senderMessage` longtext COLLATE utf8_unicode_ci NOT NULL,
  `sentAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=21 ;

--
-- Zrzut danych tabeli `ContactFormMessage`
--

INSERT INTO `ContactFormMessage` (`id`, `senderName`, `senderEmail`, `senderMessage`, `sentAt`) VALUES
(6, 'Testowy', 'przemek.kadziolka@gmail.com', 'test', '2012-01-31 04:07:18'),
(7, 'Testowa wiadomość', 'przemek.kadziolka@gmail.com', 'Test', '2012-02-02 00:12:48'),
(8, 'Marta Gołąbowicz', 'martaxxy@wp.pl', 'Dzień dobry :)\r\nChciałam zapytać, o pewien materiał ponieważ jestem bardzo zaiteresowana, Pana ofertą a konkretnie PROFILE Z/G ZAMKNIĘTE.', '2012-03-02 16:26:45'),
(9, 'Marta Gołąbowicz', 'daniel_cisak@wp.pl', 'sldfglsdfjglsdfsdfg', '2012-03-02 16:39:23'),
(10, 'Danuta Mis', 'danuta.mis@hutapokoj.eu', 'W związku z powstałą nadpłatą za wyroby hutnicze,aby dokonać zwrotu prosimy o podanie numeru konta bankowego.', '2012-10-19 08:32:54'),
(11, 'Tnji Koszty Energii', 'kontakt@tnij-koszty-energii.pl', 'Dzień dobry,\r\n\r\nczy wiesz, że możesz obniżyć koszty energii w Twojej firmie nawet o 40%? Już dziś możesz kupić energię i gaz nie ruszając się z miejsca, wystarczy że poświęcisz nam 10 minut. Dzięki temu będziesz mógł porównać koszty energii u różnych dostawców i wybrać ofertę najkorzystniejszą dla Ciebie.\r\n\r\nWięcej szczegółów znajdziesz na stronie https://tnij-koszty-energii.pl \r\n\r\n...lub po prostu odpowiedz na tę wiadomość a my skontaktujemy się z Tobą.\r\n\r\nZapraszamy!', '2018-11-11 09:35:18'),
(12, 'Oferta', 'kontakt@identyfikacja-wizualna24.pl', 'Dzień dobry,\r\n\r\njesteśmy małym, kreatywnym zespołem wydzielonym ze struktur dużej, międzynarodowej firmy. Specjalizujemy się w projektowaniu elementów wizualizacji wizualnej. Możemy stworzyć Państwu piękne, nowoczesne LOGO. Logo dla strony ac-metal.pl lub dla jakiejkolwiek innej prowadzonej przez Państwa aktywności gospodarczej czy też społecznej. \r\n\r\nLogo takie, że klienci będą kręcić głowami z podziwu a konkurencja będzie skręcać się z zazdrości :-). Równie chętnie zaprojektujemy Państwu cały pakiet elementów graficznych, takich jak WIZYTÓWKI I PAPIER FIRMOWY. Projektujemy zarówno dla wielkich firm jak i dla tych najmniejszych. Pracujemy sprawnie a nasze usługi świadczymy w atrakcyjnych i BARDZO KONKURENCYJNYCH CENACH. Stawiamy na jakość, dobre relacje i końcową satysfakcję każdego naszego klienta.\r\n\r\nJeśli są Państwo zainteresowani szczegółami, to prosimy o zwrotnego e-maila lub wypełnienie formularza na stronie https://identyfikacja-wizualna24.pl.  Aby ułatwić kontakt i przedstawienie oferty bardzo prosimy o podanie numeru telefonu.\r\n\r\nSerdecznie pozdrawiamy,\r\nPaulina, Daniel i Jarek\r\n\r\nTelefon: 794-371-189', '2018-11-21 09:43:05'),
(13, 'Randy', 'randy@talkwithlead.com', 'Hi,\r\n\r\nMy name is Randy and I was looking at a few different sites online and came across your site ac-metal.pl.  I must say - your website is very impressive.  I found your website on the first page of the Search Engine.\r\n\r\nHave you noticed that 70 percent of visitors who leave your website will never return?  In most cases, this means that 95 percent to 98 percent of your marketing efforts are going to waste, not to mention that you are losing more money in customer acquisition costs than you need to.\r\nAs a business person, the time and money you put into your marketing efforts is extremely valuable.  So why let it go to waste?  Our users have seen staggering improvements in conversions with insane growths of 150 percent going upwards of 785 percent. Are you ready to unlock the highest conversion revenue from each of your website visitors?  \r\n\r\nTalkWithLead is a widget which captures a website visitor’s Name, Email address and Phone Number and then calls you immediately, so that you can talk to the Lead exactly when they are live on your website — while they''re hot! Best feature of all, we offer FREE International Long Distance Calling!\r\n  \r\nTry the TalkWithLead Live Demo now to see exactly how it works.  Visit: http://www.talkwithcustomer.com\r\n\r\nWhen targeting leads, speed is essential - there is a 100x decrease in Leads when a Lead is contacted within 30 minutes vs being contacted within 5 minutes.\r\n\r\nIf you would like to talk to me about this service, please give me a call.  We do offer a 14 days free trial.  \r\n\r\nThanks and Best Regards,\r\nRandy\r\n\r\nIf you''d like to unsubscribe click here. http://liveserveronline.com/talkwithcustomer.aspx?d=ac-metal.pl', '2018-12-02 01:00:04'),
(14, 'Większa Sprzedaż', 'kontakt@wieksza-sprzedaz.pl', 'Dzień dobry,\r\n\r\nprzepraszam  za skorzystania z Państwa formularza kontaktowego, ale uznałam, że ten sposób będzie najprostszy i najskuteczniejszy, żeby dać Państwu i sobie szansę na ewentualną współpracę.\r\n\r\nPoniżej dosłownie kilka zdań na temat narzędzia, którego zadaniem jest ZWIĘKSZENIE EFEKTYWNOŚCI SPRZEDAŻY TOWARÓW I USŁUG oferowanych za pośrednictwem stron internetowych i e-sklepów.\r\n\r\nPROSZĘ MI WIERZYĆ, ŻE POMIMO TEGO (A MOŻE WŁAŚNIE DLATEGO), ŻE JEST OPARTE O BARDZO PROSTY W SWEJ ISTOCIE MECHANIZM, JEST NIEWIARYGODNIE SKUTECZNE.\r\n\r\nNarzędzie to zwiększa sprzedaż od kilkunastu do kilkuset procent, w zależności od branży i produktu. W naszej firmie zwiększyło ją o ponad 70%.\r\n\r\nNasz usługa nie jest kosztowna i może być zintegrowana z dowolną platformą sprzedażową lub stroną www. Jeżeli udało mi się Państwa zainteresować, to z ogromną przyjemnością (i zaangażowaniem) przybliżę Państwu szczegóły naszej oferty i ideę, o którą oparty jest mechanizm proponowanego narzędzia.\r\n\r\nProszę o zgodę na kontakt i przedstawienie oferty. Dużym ułatwieniem dla mnie będzie podanie przez Państwa MAILA i NUMERU TELEFONU.\r\n\r\np.s. WARTO ZAZNACZYĆ, ŻE WŁAŚNIE ROZPOCZĘLIŚMY SPRZEDAŻ TEJ USŁUGI NA PROMOCYJNYCH WARUNKACH (DO KOŃCA ROKU).  W chwili obecnej, to zaledwie 99 zł netto miesięcznie bez długoterminowych umów.\r\n\r\nPozdrawiam serdecznie i życzę miłego dnia.\r\n\r\nTelefon: 794-371-191', '2018-12-18 13:13:40'),
(15, 'Randy', 'randy@talkwithlead.com', 'Hi,\r\n\r\nMy name is Randy and I was looking at a few different sites online and came across your site ac-metal.pl.  I must say - your website is very impressive.  I found your website on the first page of the Search Engine.\r\n\r\nHave you noticed that 70 percent of visitors who leave your website will never return?  In most cases, this means that 95 percent to 98 percent of your marketing efforts are going to waste, not to mention that you are losing more money in customer acquisition costs than you need to.\r\nAs a business person, the time and money you put into your marketing efforts is extremely valuable.  So why let it go to waste?  Our users have seen staggering improvements in conversions with insane growths of 150 percent going upwards of 785 percent. Are you ready to unlock the highest conversion revenue from each of your website visitors?  \r\n\r\nTalkWithLead is a widget which captures a website visitor’s Name, Email address and Phone Number and then calls you immediately, so that you can talk to the Lead exactly when they are live on your website — while they''re hot! Best feature of all, we offer FREE International Long Distance Calling!\r\n  \r\nTry the TalkWithLead Live Demo now to see exactly how it works.  Visit: http://www.talkwithcustomer.com\r\n\r\nWhen targeting leads, speed is essential - there is a 100x decrease in Leads when a Lead is contacted within 30 minutes vs being contacted within 5 minutes.\r\n\r\nIf you would like to talk to me about this service, please give me a call.  We do offer a 14 days free trial.  \r\n\r\nThanks and Best Regards,\r\nRandy\r\n\r\nIf you''d like to unsubscribe click here. http://liveserveronline.com/talkwithcustomer.aspx?d=ac-metal.pl', '2019-01-06 10:55:10'),
(16, 'Strony WWW', 'kontakt@strony-www24.pl', 'Dzień dobry,\r\n\r\nprzede wszystkim mam nadzieję, że wybaczą mi Państwo taką właśnie formę skontaktowania się z Państwem, ale nie znalazłam lepszej, umożliwiającej podjęcie choćby próby zainteresowania nasza ofertą. Bardzo proszę o wyrozumiałość. \r\n\r\nPracuję wraz z grupą cudownych, bardzo zdolnych i kreatywnych młodych ludzi. Nasz zespół projektuje, wykonuje i wdraża strony www i sklepy dla klientów z całego świata, ze szczególnym wskazaniem klientów z Polski.\r\n\r\nNasza firma w związku z dynamicznym rozwojem w Polsce, ma do zaoferowania niezwykle atrakcyjne ceny na wiele produktów związanych z szeroko rozumianym marketingiem internetowym, a w szczególności dotyczy to cen na zaprojektowanie, wykonanie i wdrożenie: stron www, sklepów internetowych, identyfikacji wizualnej (logotypów, wizytówek, itp.).\r\n\r\nPonieważ wiem, że oferta naprawdę jest bardzo atrakcyjna (nie tylko cenowo), to z dużą nieśmiałością chciałabym zapytać Państwa, czy nie dalibyście się Państwo przekonać do gruntownego odświeżenia Państwa internetowego wizerunku, polegającego na zaprojektowaniu od nowa serwisu ac-metal.pl z uwzględnieniem wszystkich najnowszych rozwiązań programistycznych i trendów graficznych.\r\n\r\nJeżeli zgadzacie się Państwo dać mi szansę i pozwolić, żebym nieco szerzej przedstawiła naszą ofertę, to proszę o potwierdzenie tego faktu poprzez odpowiedź na niniejszego maila. Będę wdzięczna również za podanie numeru telefonu na jaki mogłabym zadzwonić. To zdecydowanie ułatwi nam kontakt. \r\n\r\nDodatkowe informacje znajdziecie Państwo na stronie: https://strony-www24.pl\r\n\r\nPozdrawiam serdecznie,\r\ne-mail: kontakt@strony-www24.pl\r\ntelefon: 536-410-956', '2019-01-21 10:40:59'),
(17, 'Katarzyna', 'kontakt@internetowa-reklama.pl', 'Dzień dobry,\r\n\r\nJestem osobą pracującą w firmie od lat prowadzącej działania reklamowe, których celem jest zwiększenie skuteczności i efektywności funkcjonujących w Internecie sklepów i stron internetowych. Specjalizujemy się między innymi w zarządzaniu kampaniami reklamowymi w serwisie Facebook, prowadzenimy na zlecenie naszych klientów ich firmowe profile na tym portalu, a także prowadzimy kampanie w Google.\r\n\r\nNie ukrywam, że moją intencją jest namówienie Państwa na skorzystanie z naszej oferty w tym właśnie zakresie. Jeśli chcecie Państwo zwiększyć ruch w serwisie ac-metal.pl, jeśli chcecie, żeby ta zwiększona ilość wizyt przyniosła spodziewane efekty i zwiększyła sprzedaż Państwa towarów i/lub usług, to bez wątpienia potrafimy w tym skutecznie pomóc. Jesteśmy w tym naprawdę dobrzy. Jesteśmy też bardzo skuteczni. Mamy „rozsądne” a nawet „bardzo rozsądne” ceny. \r\n\r\nJeżeli dacie mi Państwo szansę, to chętnie bliżej przedstawię naszą ofertę. W razie zainteresowania uprzejmie proszę o odpowiedź na tę wiadomość i zachęcam do podania numeru telefonu, który umożliwi mi przedstawienie oferty w sposób, który będzie dla Państwa wygodny.\r\n\r\nPozdrawiam serdecznie.\r\n\r\ntel.: 536-410-956\r\ne-mail: kontakt@internetowa-reklama.pl', '2019-02-11 12:50:15'),
(18, 'Paweł', 'kontakt@strony-www24.pl', 'Dzień dobry,\r\n\r\nreprezentuję firmę, która od lat tworzy profesjonalne strony internetowe. Odwiedziłem Państwa stronę ac-metal.pl i w związku z tym chciałbym zapytać czy są Państwa zainteresowani stworzeniem nowej strony www? W ciągu ostatnich lat bardzo mocno zmieniły się zasady wykonywania stron internetowych, dotyczące między innymi ich mobilności czy szybkości działania. Strony wykonujemy na bardzo atrakcyjnych warunkach, również finansowych.\r\n\r\nJeżeli zgadzacie się Państwo na to abym szerzej przedstawił ofertę, to proszę o odpowiedź na niniejszego maila. Zachęcam do podania numeru telefonu na jaki mógłbym zadzwonić. To zdecydowanie ułatwi nam kontakt.\r\n\r\nPozdrawiam serdecznie,\r\nPaweł Kłos\r\ntel.: 536-410-956\r\nkontakt@strony-www24.pl', '2019-02-28 08:55:54'),
(19, 'Marek', 'kontakt@email-reklama.pl', 'Dzień dobry,\r\n\r\nzdając sobie sprawę z tego, że pozyskiwanie nowych klientów jest w dzisiejszych czasach trudne chciałbym zaproponować Państwa firmie bardzo skuteczną formę reklamy - kampanie e-mail marketingową. Z naszej oferty korzystają zarówno duże firmy jak i jednoosobowe działalności gospodarcze, skutecznie sprzedając swoje usługi i produkty. Mamy atrakcyjne ceny uzależnione od wielkości kampanii reklamowej, dzięki czemu jesteśmy w stanie dostosować się do oczekiwań naszych klientów.\r\n\r\nJeżeli zgadzacie się Państwo na to abym szerzej przedstawił naszą ofertę, to proszę o odpowiedź na tę wiadomość. Zachęcam do podania numeru telefonu na jaki mógłbym zadzwonić. To zdecydowanie ułatwi nam kontakt.\r\n\r\nPozdrawiam serdecznie,\r\nMarek Jakubiak\r\ntel.: 536-410-956\r\nkontakt@email-reklama.pl', '2019-06-06 10:11:07'),
(20, 'Marcin', 'marcin@internetowy-wizerunek.pl', 'Szanowni Państwo!\r\n\r\nproszę mi wybaczyć tę formę komunikacji, ale nie bardzo mogę sobie wyobrazić, w jaki inny sposób mógłbym zapytać o to, czy jesteście Państwo zainteresowani zaprojektowaniem całkowicie od nowa, posiadanej strony internetowej ac-metal.pl.\r\n\r\nSądzę, że byłoby to świetną okazją do odświeżenia Państwa internetowego wizerunku oraz wdrożenia nowoczesnych rozwiązań technicznych uwzględniających także optymalizację strony pod kątem jej funkcjonowania w świecie urządzeń mobilnych, szybkości działania i pozycjonowania (SEO).\r\n\r\nJesteśmy firmą o wieloletnim, międzynarodowym doświadczeniu, specjalizującą się w kompleksowym tworzeniu i wdrażaniu STRON WWW oraz SKLEPÓW INTERNETOWYCH.\r\n\r\nChętnie przedstawię Państwu dokładniej naszą propozycję. Jeżeli jesteście Państwo zainteresowani, to proszę o odpowiedź na niniejszego maila. Byłbym wdzięczny, gdyby w odpowiedzi podali Państwo NUMER TELEFONU KONTAKTOWEGO, na który mógłbym zadzwonić. To bardzo ułatwi ewentualne przygotowanie doskonale dopasowanej do Państwa oczekiwań propozycji.\r\n\r\nZ wyrazami szacunku,\r\n\r\nMarcin Maceja\r\ntel.: 536-410-956\r\n\r\ne-mail: marcin@internetowy-wizerunek.pl\r\nwww: https://internetowy-wizerunek.pl', '2019-06-07 10:27:27');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `fos_group`
--

CREATE TABLE IF NOT EXISTS `fos_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_4B019DDB5E237E06` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Zrzut danych tabeli `fos_group`
--

INSERT INTO `fos_group` (`id`, `name`, `roles`) VALUES
(1, 'Administratorzy', 'a:1:{i:0;s:16:"ROLE_SUPER_ADMIN";}');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `fos_user`
--

CREATE TABLE IF NOT EXISTS `fos_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Zrzut danych tabeli `fos_user`
--

INSERT INTO `fos_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`, `created_at`, `updated_at`, `first_name`, `last_name`) VALUES
(4, 'admin', 'admin', 'test@acme.com', 'test@acme.com', 1, 'pjo60nsowa8s8ksg808c0c4k04wg0g0', 'e73158218f3edfccfde7727f846009559cce2e2473f305e0e22a5d222753e6776cf9ae54527821d9190acd2694213530d1f3dcd77cb89999648b7d4757b5358b', '2016-05-26 00:21:29', 0, 0, NULL, '4j5xpumaqug4ocwkk0o84s0s8gwkssg40ccccc8wgo4sw8880o', NULL, 'a:1:{i:0;s:16:"ROLE_SUPER_ADMIN";}', 0, NULL, '2012-01-26 23:31:55', '2016-05-26 00:21:29', NULL, NULL),
(5, 'przemq', 'przemq', 'przemek@meble.pl', 'przemek@meble.pl', 1, 'hvpzazwd0rsoossccw4okwk0ko8s004', 'aa266879f2a1be41486c612ca28653bb14deecbae306e1fcda9d047a0fa48237553ba3c299a28c6a7d2cd10ec56f812b18a4011e6d676f4a9da5631b44dad1a2', '2012-01-27 02:35:55', 0, 0, NULL, '32dsbuvazmo080w8kosw8co40sw448kkk8w0ws8800kskc84kc', NULL, 'a:1:{i:0;s:10:"ROLE_ADMIN";}', 0, NULL, '2012-01-27 02:21:51', '2012-01-30 10:07:09', 'przemek', NULL),
(6, 'daniel', 'daniel', 'daniel_cisak@wp.pl', 'daniel_cisak@wp.pl', 1, 'n1crm6ikyuosso8coso8s0kwsg4o0k8', 'c092d6bf193e8655583a955e05f82c586ed41b3a035dcae377c30f224962db2ea406c81c39ba035ce8a202078117cabad0df26c7e588af9075438babae1dadb2', '2012-02-03 00:09:00', 0, 0, NULL, '3w4tpw3by6m8gcso44gg8kkwkowwcwks40g4sko4cc4cw4cs8c', NULL, 'a:0:{}', 0, NULL, '2012-02-02 00:07:29', '2012-02-03 00:09:00', 'Daniel', 'Cisak');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `fos_user_group`
--

CREATE TABLE IF NOT EXISTS `fos_user_group` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`group_id`),
  KEY `IDX_583D1F3EA76ED395` (`user_id`),
  KEY `IDX_583D1F3EFE54D947` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `fos_user_group`
--

INSERT INTO `fos_user_group` (`user_id`, `group_id`) VALUES
(6, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `menu_item`
--

CREATE TABLE IF NOT EXISTS `menu_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) DEFAULT NULL,
  `anchor` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `url` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attributes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D754D550C4663E4` (`page_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Zrzut danych tabeli `menu_item`
--

INSERT INTO `menu_item` (`id`, `page_id`, `anchor`, `title`, `type`, `location`, `sort_order`, `url`, `attributes`) VALUES
(1, 4, 'Kontakt', 'Kontakt', 'page', 'bottom', 3, 'http://www.googl', NULL),
(2, 5, 'Oferta', 'Oferta', 'page', 'bottom', 2, 'http://www.google.com', NULL),
(3, 3, 'O firmie', 'O firmie', 'page', 'bottom', 0, NULL, NULL),
(4, 7, 'Usługi', 'Usługi', 'page', 'bottom', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `page`
--

CREATE TABLE IF NOT EXISTS `page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `published` tinyint(1) DEFAULT NULL,
  `publish_date` datetime DEFAULT NULL,
  `keywords` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(2048) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `automaticSeo` tinyint(1) NOT NULL,
  `topImage` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `onMainPage` tinyint(1) DEFAULT NULL,
  `onMainPageOrder` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Zrzut danych tabeli `page`
--

INSERT INTO `page` (`id`, `title`, `published`, `publish_date`, `keywords`, `description`, `price`, `slug`, `content`, `automaticSeo`, `topImage`, `onMainPage`, `onMainPageOrder`) VALUES
(3, 'O firmie', 1, '2007-01-01 00:00:00', 'AC-METAL, materiały hutnicze, handel', 'O firmie AC-METAL', 0.00, 'o-firmie', '<p><strong>AC-METAL</strong> jest firmą o profilu handlowo-usługowym. Gł&oacute;wnym&nbsp;przedmiotem naszej działalności jest handel wyrobami&nbsp;hutniczymi posiadającymi atesty. Dzięki temu mają Państwo&nbsp;pewność najwyższej jakości oferowanego asortymentu.</p>\r\n<p><br />Oferujemy szeroki zakres wyrob&oacute;w hutniczych, m.in.: rury,&nbsp;blachy, pręty (walcowane, ciągnione, gorąco walcowane,&nbsp;ciągnione na zimno, łuszczone); w gatunkach S235, S355, 45,&nbsp;40H, stale narzędziowe i do nawęglania.</p>\r\n<p><br />Naszym gł&oacute;wnym atutem są liczne kontakty handlowe, dzięki&nbsp;kt&oacute;rym mamy dostęp do ogromnej r&oacute;żnorodności towar&oacute;w w&nbsp;niezwykle atrakcyjnej i konkurencyjnej cenie. Nawiązujemy&nbsp;wsp&oacute;łpracę z firmami z odległych zakątk&oacute;w kraju. Zajmujemy&nbsp;się zlecaniem usług w zakresie cięcia, gięcia, spawania&nbsp;i cynkowania. Sprzedaż realizujemy przez bezpośrednie&nbsp;dostawy z hut krajowych i zagranicznych (Słowacja, Ukraina).&nbsp;Dysponujemy własnym transportem, dzięki czemu zam&oacute;wione&nbsp;materiały dostarczamy bezpośrednio do Klienta.</p>\r\n<p><br />Zapraszam do wsp&oacute;łpracy,</p>\r\n<p><br />Andrzej Cisak</p>', 0, '4f285d229874e.jpg', 1, 0),
(4, 'Kontakt', 1, '2007-01-01 00:00:00', 'Kontakt AC-METAL', 'Skontaktuj się z firmą AC-METAL za pomocą formularza kontaktowego', 0.00, 'kontakt', '<div style="width: 490px;">\r\n<p><strong>AC METAL</strong> Andrzej Cisak</p>\r\n<p><br /><br />84-300 Lębork, ul. Pionier&oacute;w 17</p>\r\n<p><br />tel./fax <strong>59 863 11 39</strong>, kom. <strong>604 40 28 28</strong></p>\r\n<p>e-mail:<strong> a.cisak@ac-metal.pl</strong>, <strong>www.ac-metal.pl</strong></p>\r\n<p><br />NIP 841 001 28 34, REGON 770703372</p>\r\n<p>&nbsp;</p>\r\n<p><a class="button1" style="display: inline-block; line-height: 20px;" href="http://maps.google.com/maps?q=84-300+L%C4%99bork,+ul.+Pionier%C3%B3w+17&amp;hl=pl&amp;ie=UTF8&amp;ll=54.547575,17.721591&amp;spn=0.011127,0.032508&amp;sll=54.544642,17.753251&amp;sspn=0.08902,0.260067&amp;hnear=Pionier%C3%B3w+17,+L%C4%99bork,+l%C4%99borski,+Pomorskie,+Polska&amp;t=m&amp;z=16&amp;iwloc=A">Zobacz nas na mapach google</a></p>\r\n</div>', 0, '4f2861741280c.jpg', 1, 3),
(5, 'Oferta', 1, '2007-01-01 00:00:00', 'Ac-Metal, oferta', 'Oferta firmy AC-METAL', 0.00, 'oferta', '<p><strong>Oferujemy:</strong></p>\r\n<ul>\r\n<li>BLACHY,TAŚMY,BEDNARKI</li>\r\n<li>KĄTOWNIKI,KĄTOWNIKI, CEOWNIKI, DWUTEOWNIKI ,TEOWNIKI</li>\r\n<li>PRĘTY GŁADKIE, KWADRATOWE,PŁASKIE, SZEŚCIOKĄTNE</li>\r\n<li>PRĘTY</li>\r\n<li>RURY ZE SZWEM, BEZ SZWU</li>\r\n<li>PROFILE Z/G ZAMKNIĘTE</li>\r\n<li>KĘSY</li>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<p>Pełną ofertą można pobrać <span style="color: #ff0000;"><a href="oferta.pdf"><span style="color: #ff0000;">tutaj.</span></a></span></p>', 0, '4f286543ed8a4.jpg', 1, 2),
(7, 'Usługi', 1, '2012-01-30 20:40:00', 'usługi, AC-METAL', 'Usługi oferowane przez firmę AC-METAL', 0.00, 'uslugi', '<p><strong>Dla spełnienia oczekiwań naszych Klient&oacute;w</strong> i zapewnienia&nbsp;kompleksowej sprzedaży wsp&oacute;łpracujemy z partnerami z&nbsp;branż pochodnych, dzięki czemu jesteśmy w stanie zapewnić&nbsp;dodatkowe usługi, m.in.:</p>\r\n<ul>\r\n<li>usługi transportowe</li>\r\n<li>usługi dla zakupionych produkt&oacute;w:\r\n<ul>\r\n<li>cięcia</li>\r\n<li>gięcia</li>\r\n<li>spawania</li>\r\n<li>wykonywanie gotowych element&oacute;w wg dostarczonego&nbsp;projektu</li>\r\n</ul>\r\n</li>\r\n</ul>', 0, '4f28654e56366.jpg', 1, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pages_tags`
--

CREATE TABLE IF NOT EXISTS `pages_tags` (
  `page_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`page_id`,`tag_id`),
  KEY `IDX_2476DEA6C4663E4` (`page_id`),
  KEY `IDX_2476DEA6BAD26311` (`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `tag`
--

CREATE TABLE IF NOT EXISTS `tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `fos_user_group`
--
ALTER TABLE `fos_user_group`
  ADD CONSTRAINT `FK_583D1F3EA76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`),
  ADD CONSTRAINT `FK_583D1F3EFE54D947` FOREIGN KEY (`group_id`) REFERENCES `fos_group` (`id`);

--
-- Ograniczenia dla tabeli `menu_item`
--
ALTER TABLE `menu_item`
  ADD CONSTRAINT `FK_D754D550C4663E4` FOREIGN KEY (`page_id`) REFERENCES `page` (`id`);

--
-- Ograniczenia dla tabeli `pages_tags`
--
ALTER TABLE `pages_tags`
  ADD CONSTRAINT `FK_2476DEA6BAD26311` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_2476DEA6C4663E4` FOREIGN KEY (`page_id`) REFERENCES `page` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
