<?php

namespace XD\CmsBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

/**
 * Description of Builder
 *
 * @author przemq
 */
class Builder extends ContainerAware {

    public function managedMenu(FactoryInterface $factory) {

        $menu = $factory->createItem('root');

        $request = $this->container->get('request');
        $translator = $this->container->get('translator');
        $doctrine = $this->container->get('doctrine');

		
        $menu->setCurrentUri($this->container->get('request')->getRequestUri());
        
        $em = $this->container->get('doctrine')->getEntityManager();
        
        $location = 'bottom';
        
        $menu_items = $em->createQuery("
        		SELECT m 
        		FROM XDCmsBundle:MenuItem m
        		LEFT JOIN m.page p
        		WHERE 
        			m.location = :location
        		ORDER BY m.sortOrder ASC, m.title ASC")
        ->setParameter('location', $location)
        ->execute();
        
        
        
        
  		foreach($menu_items as $item) {
  			
  			
  			
  			switch($item->getType()) {
  				case 'url':
  					$menu -> addChild($item->getTitle(), array(
  						'uri' => $item->getUrl(), 
  						'linkAttributes' => $item->getAttributesArray()));
  					break;
  					
  				case 'page':
  					if($item->getPage()->isPublished()) {
  						
  						$menu->addChild($item->getTitle(), array(
  								'route' => $item->getPage()->getOnMainPage() ? 'acmetal_page_show' :'page_show',
  								'linkAttributes' => $item->getAttributesArray(),
  								'routeParameters' => array('slug' => $item->getPage()->getSlug())
  						));
  					}
		            break;
  				default:
  					
  			}
  			
  			
  		}

  
//         foreach ($pages as $page) {

//             $menu->addChild($page->getTitle(), array(
//                 'route' => 'page_show',
//                 'routeParameters' => array('slug' => $page->getSlug())
//             ));
            
//         }

        return $menu;
    }

}
