<?php

namespace XD\CmsBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\FixtureInterface;
use \XD\CmsBundle\Entity\Page;


class LoadPageData implements FixtureInterface {
    
    /**
     *
     * @param EntityManager $manager 
     */
    function load($manager) {
        
        $page1 = new Page();
        
        $page1->setTitle('First simple page');
        $page1->setSlug('first-simple-page');
        $page1->setKeywords('first simple page');
        $page1->setDescription('This is very first - simple page');
        $page1->setContent('
<p> This is very first page containing raw HTML </p>

<p> Second paragraph </p>

<ul>
    <li>List first element</li>
    <li>List second element</li>
    <li>List third element</li>
</ul>

<p> A link to very nice web page: <a href="http://www.google.com">google.com</a> (search engine) :D </p>



');

        $manager->persist($page1);
        $manager->flush();
        
    }
    
}