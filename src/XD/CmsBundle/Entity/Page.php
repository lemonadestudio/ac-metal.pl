<?php

namespace XD\CmsBundle\Entity;
use XD\CmsBundle\Helper\Cms;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

use XD\CmsBundle\Entity\Tag;

/**
 * 
 * XD\CmsBundle\Entity\Page
 *
 */
class Page {
	/**
	 * @var integer $id
	 *
	 */
	private $id;

	/**
	 * @var string $title
	 *
	 */
	private $title;

	/**
	 * 
	 * @var boolean
	 * 
	 */
	private $published = true;

	/**
	 *
	 * @var \DateTime  $publishDate;
	 * 
	 */
	private $publishDate;

	/**
	 * @var string $keywords
	 *
	 */
	private $keywords;

	/**
	 * @var string $description
	 *
	 */
	private $description;

	/**
	 *
	 * @var Collection
	 *
	 */
	private $tags;

	/**
	 *
	 * @var decimal
	 * 
	 */
	private $price = 0.00;

	/**
	 * @var string $slug
	 * 
	 *
	 */
	private $slug;

	/**
	 * @var text $content
	 *
	 */
	private $content;
	
	public $file;

	public function __construct() {

		$this->tags = new ArrayCollection();
		$this->isPublished = true;
		$this->setAutomaticSeo(true);
		$this->setPublishDate(new \DateTime());
		$this->onMainPage = true;

	}

	/**
	 * Get id
	 *
	 * @return integer 
	 */
	public function getId() {
		return $this->id;
	}

	public function getPrice() {
		return $this->price;
	}

	public function setPrice($price) {
		$this->price = $price;
	}

	public function addTag(Tag $tag) {

		$tag->addPage($this);
		$this->tags[] = $tag;

	}

	public function getTags() {
		return $this->tags;
	}

	public function setTags(ArrayCollection $tags) {
		$this->tags = $tags;
	}

	/**
	 * Set title
	 *
	 * @param string $title
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * Get title
	 *
	 * @return string 
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * Set publish date
	 *
	 * @param \DateTime $publish_date
	 */
	public function setPublishDate($publish_date) {
		$this->publishDate = $publish_date;
	}

	/**
	 * Get publishDate
	 *
	 * @return \DateTime
	 */
	public function getPublishDate() {
		return $this->publishDate;
	}

	/**
	 * Set keywords
	 *
	 * @param string $keywords
	 */
	public function setKeywords($keywords) {
		$this->keywords = $keywords;
	}

	/**
	 * Get keywords
	 *
	 * @return string 
	 */
	public function getKeywords() {
		return $this->keywords;
	}

	/**
	 * Set description
	 *
	 * @param string $description
	 */
	public function setDescription($description) {
		$this->description = $description;
	}

	/**
	 * Get description
	 *
	 * @return string 
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * Set slug
	 *
	 * @param string $slug
	 */
	public function setSlug($slug) {
		$this->slug = $slug;
	}

	/**
	 * Get slug
	 *
	 * @return string 
	 */
	public function getSlug() {
		return $this->slug;
	}

	/**
	 * Set content
	 *
	 * @param text $content
	 */
	public function setContent($content) {
		$this->content = $content;
	}

	/**
	 * Get content
	 *
	 * @return text 
	 */
	public function getContent() {
		return $this->content;
	}

	public function setPublished($published) {
		$this->published = $published;
	}

	public function isPublished() {
		return $this->published;
	}
	
    /**
     * @var boolean $automaticSeo
     */
    private $automaticSeo;


    /**
     * Get published
     *
     * @return boolean 
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * Set automaticSeo
     *
     * @param boolean $automaticSeo
     */
    public function setAutomaticSeo($automaticSeo)
    {
        $this->automaticSeo = $automaticSeo;
    }

    /**
     * Get automaticSeo
     *
     * @return boolean 
     */
    public function getAutomaticSeo()
    {
        return $this->automaticSeo;
    }
    
    public function __toString() {
    	return substr($this->getTitle(), 0, 100);
    }

    public function setCreatedValue()
    {
        
    	$cms_helper = new Cms();
    	
    	
    	if($this->getAutomaticSeo()) {
    		
    		// urla
    		$this->setSlug($cms_helper->make_url($this->getTitle()));
    		
    		
    		// description 
    		$description = strip_tags($this->getContent());
    		$description = mb_substr(html_entity_decode($description), 0, 2048);

    		
    		// usunięcie ostatniego, niedokończonego zdania
    		
    		
			$description = preg_replace('@(.*)\..*@', '\1', $description);
    		
    		$this->setDescription($description);
    		
    		// keywords
    		$keywords_arr = explode(' ', $this->getTitle().' '.$this->getDescription());

    		$keywords = array();
    		if(is_array($keywords_arr)) {
    			foreach($keywords_arr as $kw) {
    				$kw = trim($kw);
    				$kw = preg_replace('@\.,;\'\"@', '', $kw);
    				if(strlen($kw) >= 4 && !in_array($kw, $keywords)) {
    					$keywords[] = $kw;
    				}
    				if(count($keywords) >= 10) {
    					break;
    				}
    			}
    		}
    		
    		$this->setKeywords(implode(', ', $keywords));
    		
    		$this->setAutomaticSeo(false);
    		
    	}
    	
    	
    }
    /**
     * @var string $topImage
     */
    private $topImage;


    /**
     * Set topImage
     *
     * @param string $topImage
     */
    public function setTopImage($topImage)
    {
        $this->topImage = $topImage;
    }

    /**
     * Get topImage
     *
     * @return string 
     */
    public function getTopImage()
    {
        return $this->topImage;
    }
    
    
    public function getAbsolutePath()
    {
    	return null === $this->getTopImage() ? null : $this->getUploadRootDir().'/'.$this->getTopImage();
    }
    
    public function getWebPath()
    {
    	return null === $this->getTopImage() ? null : $this->getUploadDir().'/'.$this->getTopImage();
    }
    
    protected function getUploadRootDir()
    {
    	
    	// the absolute directory path where uploaded documents should be saved
    	return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }
    
    protected function getUploadDir()
    {
    	// get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
    	return 'uploads/page';
    }
    
    
    /**
     */
    public function preUpload()
    {
    	if (null !== $this->file) {
    		// do whatever you want to generate a unique name
    		$this->oldImage = $this->getAbsolutePath();
    		$this->topImage = uniqid().'.'.$this->file->guessExtension();
    		
    	}
    }
    
    /**
     */
    public function upload()
    {
    	if (null === $this->file) {
    		return;
    	}
    
    	// if there is an error when moving the file, an exception will
    	// be automatically thrown by move(). This will properly prevent
    	// the entity from being persisted to the database on error
    	$this->file->move($this->getUploadRootDir(), $this->topImage);
    	
    	if($this->oldImage) {
    		unlink($this->oldImage);
    	}
    
    	unset($this->file);
    }
    
    /**
     */
    public function removeUpload()
    {
    	if ($file = $this->getAbsolutePath()) {
    		unlink($file);
    	}
    }
    
    /**
     * @var boolean $onMainPage
     */
    private $onMainPage;


    /**
     * Set onMainPage
     *
     * @param boolean $onMainPage
     */
    public function setOnMainPage($onMainPage)
    {
        $this->onMainPage = $onMainPage;
    }

    /**
     * Get onMainPage
     *
     * @return boolean 
     */
    public function getOnMainPage()
    {
        return $this->onMainPage;
    }
    /**
     * @var integer $onMainPageOrder
     */
    private $onMainPageOrder;


    /**
     * Set onMainPageOrder
     *
     * @param integer $onMainPageOrder
     */
    public function setOnMainPageOrder($onMainPageOrder)
    {
        $this->onMainPageOrder = $onMainPageOrder;
    }

    /**
     * Get onMainPageOrder
     *
     * @return integer 
     */
    public function getOnMainPageOrder()
    {
        return $this->onMainPageOrder;
    }
    
    public function getUniqid() {
    	return substr(md5($this->getId()), 10, 10);
    }
    
    public function hasContactForm() {
    	if(preg_match('@kontakt@', $this->slug)) {
    		return true;
    	}
    	return false;
    }
}