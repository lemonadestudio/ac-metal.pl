<?php

namespace XD\CmsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

use XD\CmsBundle\Entity\Page;

/**
 * @author przemq
 * 
 */
class Tag {
    
    /**
     *
     * @var type 
     * 
     */
    private $id;
    
    /**
     *
     * @var string 
     * 
     */
    private $tag;
    
    /**
     *
     * @var Collection
     * 
     */
    private $pages;
    
    
    public function __construct() {
        
        $this->pages = new ArrayCollection();
        
    }
    
    public function addPage(Page $page) {
        
        $this->pages[] = $page;
        
    }
    
    public function getTag() {
        return $this->tag;
    }

    public function setTag($tag) {
        $this->tag = $tag;
    }
    
    public function __toString() {
        return $this->tag;
    }



    
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get pages
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getPages()
    {
        return $this->pages;
    }
}