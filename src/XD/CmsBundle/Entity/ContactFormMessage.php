<?php

namespace XD\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * XD\CmsBundle\Entity\ContactFormMessage
 */
class ContactFormMessage
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $senderName
     */
    private $senderName;

    /**
     * @var string $senderEmail
     */
    private $senderEmail;

    /**
     * @var text $senderMessage
     */
    private $senderMessage;

    /**
     * @var datetime $sentAt
     */
    private $sentAt;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set senderName
     *
     * @param string $senderName
     */
    public function setSenderName($senderName)
    {
        $this->senderName = $senderName;
    }

    /**
     * Get senderName
     *
     * @return string 
     */
    public function getSenderName()
    {
        return $this->senderName;
    }

    /**
     * Set senderEmail
     *
     * @param string $senderEmail
     */
    public function setSenderEmail($senderEmail)
    {
        $this->senderEmail = $senderEmail;
    }

    /**
     * Get senderEmail
     *
     * @return string 
     */
    public function getSenderEmail()
    {
        return $this->senderEmail;
    }

    /**
     * Set senderMessage
     *
     * @param text $senderMessage
     */
    public function setSenderMessage($senderMessage)
    {
        $this->senderMessage = $senderMessage;
    }

    /**
     * Get senderMessage
     *
     * @return text 
     */
    public function getSenderMessage()
    {
        return $this->senderMessage;
    }

    /**
     * Set sentAt
     *
     * @param datetime $sentAt
     */
    public function setSentAt($sentAt)
    {
        $this->sentAt = $sentAt;
    }

    /**
     * Get sentAt
     *
     * @return datetime 
     */
    public function getSentAt()
    {
        return $this->sentAt;
    }
}