<?php

namespace XD\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * XD\CmsBundle\Entity\MenuItem
 */
class MenuItem
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $anchor
     */
    private $anchor;

    /**
     * @var string $title
     */
    private $title;

    /**
     * @var string $type
     */
    private $type;


    /**
     * @var string $url
     */
    private $url;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set anchor
     *
     * @param string $anchor
     */
    public function setAnchor($anchor)
    {
        $this->anchor = $anchor;
    }

    /**
     * Get anchor
     *
     * @return string 
     */
    public function getAnchor()
    {
        return $this->anchor;
    }

    /**
     * Set title
     *
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set type
     *
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }



    /**
     * Set url
     *
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }
    
    /**
     * @var XD\CmsBundle\Entity\Page
     */
    private $page;


    /**
     * Set page
     *
     * @param XD\CmsBundle\Entity\Page $page
     */
    public function setPage(\XD\CmsBundle\Entity\Page $page)
    {
        $this->page = $page;
    }

    /**
     * Get page
     *
     * @return XD\CmsBundle\Entity\Page 
     */
    public function getPage()
    {
        return $this->page;
    }
    /**
     * @var string $location
     */
    private $location;

    /**
     * @var integer $sortOrder
     */
    private $sortOrder;


    /**
     * Set location
     *
     * @param string $location
     */
    public function setLocation($location)
    {
        $this->location = $location;
    }

    /**
     * Get location
     *
     * @return string 
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
    }

    /**
     * Get sortOrder
     *
     * @return integer 
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }
    /**
     * @var array $attributes
     */
    private $attributes;


    /**
     * Set attributes
     *
     * @param array $attributes
     */
    public function setAttributes($attributes)
    {
        $this->attributes = $attributes;
    }

    /**
     * Get attributes
     *
     * @return array 
     */
    public function getAttributes()
    {
        return $this->attributes;
    }
    
    public function getAttributesArray() {
    	
    	try {
	    	 $dom = new \DOMDocument();
	    	 $dom->loadHTML('<a '.$this->getAttributes().'></a>');
	    	 $s = simplexml_import_dom($dom);
	    	 $aa=(array)$s->body->a;
	    	 
    	} catch (Exception $e) {
    		return array();
    	}
    	
    	 if(is_array($aa) && array_key_exists('@attributes', $aa)) {
    	 	return $aa['@attributes'];
    	 }
    	 
    	 return array();
	     
    }
}