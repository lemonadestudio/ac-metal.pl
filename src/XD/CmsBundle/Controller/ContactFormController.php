<?php

namespace XD\CmsBundle\Controller;

use Behat\Mink\Session;

use Symfony\Component\Validator\Constraints\MaxLength;

use Symfony\Bundle\DoctrineBundle\Registry;

use Symfony\Component\Validator\Constraints\NotBlank;

use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\MinLength;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use XD\CmsBundle\Entity\ContactFormMessage;

class ContactFormController extends Controller
{

	 /**
     *
     * @Route("/formularz-kontaktowy", name="contact_form")
     * 
     */
    public function showAction($included = false)
    {
    	
    
    	$request = $this->getRequest();
    	
    	$message = new ContactFormMessage();
    	
    	$collectionConstraint = new Collection(array(
    			'senderName' => array(new NotBlank(), new MinLength(5), new MaxLength(100)),
    			'senderEmail' => array(new Email(array('message' => 'Invalid email address')), new MaxLength(200)),
    			'senderMessage' => array(new NotBlank(), new MaxLength(4000))
    	));
    	
    	// create a form, no default values, pass in the constraint option
    	
    	
    	$form = $this->createFormBuilder(
    			null, 
    			array(
    				'validation_constraint' => $collectionConstraint)
    			)
    		->add('senderName', 'text')
	    	->add('senderEmail', 'email')
	    	->add('senderMessage', 'textarea', array('required' => false))
	    	->add('captcha', 'captcha',  array("property_path" => false))
	    	
    	->getForm();
    	
    	if ($request->getMethod() == 'POST') {
    		$form->bindRequest($request);
    	
    		if ($form->isValid()) {
    			
    			// perform some action, such as saving the task to the database
    	
    			$data = $form->getData();
    			
    			$message = new ContactFormMessage();
    			$message->setSenderEmail($data['senderEmail']);
    			$message->setSenderMessage($data['senderMessage']);
    			$message->setSenderName($data['senderName']);
    			$message->setSentAt(new \DateTime());
    			
    			$em = $this->get('doctrine')->getEntityManager();
    			$em->persist($message);
    			$em->flush();

    			
    			
    			$message = \Swift_Message::newInstance()
	    			->setSubject('Nowa wiadomość z formularza kontaktowego od '.$data['senderName'])
	    			->setFrom($data['senderEmail'])
	    			->setTo($this->container->getParameter('mailer_user'))
	    			->setBody('Treść wiadomości: '.$data['senderMessage'])
    			;
    			
    			try {
    				$ret = $this->get('mailer')->send($message);
    			} catch(Exception $e) {
    				
    			}
    		
				
    		
    			
    			$this->get('session')->setFlash('contact_form_sent', 'Thanks, your message has been sent!');
    			 
    			return $this->redirect($this->generateUrl('contact_form'));
    			
    		}
    	}
    	 
    	
    	if($included != false) {

    		$response = $this->render('XDCmsBundle:ContactForm:include_show.html.twig', array(
    				'form' => $form->createView(),
    		));
    		$response->setSharedMaxAge(0);
    		
    		return $response;
    		
    	}
    	
    	return $this->render('XDCmsBundle:ContactForm:show.html.twig', array(
    			'form' => $form->createView(),
    	));
        
    }
    
    
    
}
