<?php

namespace XD\CmsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class PageController extends Controller
{
    /**
     *
     * // @ Route(name="page_list")
     */
    public function listAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
        
        $query = $em->createQuery("SELECT p FROM XDCmsBundle:Page p ORDER BY p.id DESC");
        
        $pages = $query->getResult();
        
        $response = $this->render('XDCmsBundle:Page:list.html.twig', array('pages' => $pages));
        
        $response->setSharedMaxAge(10);
        $response->setMaxAge(10);

        return $response;
        
    }
    
    /**
     *
     * @Route("/{slug},p.html", name="page_show")
     * 
     * 
     * @param string $slug
     * @return array params 
     */
    public function showAction($slug) {
        
        $em = $this->getDoctrine()->getEntityManager();
        
        $page = $em->getRepository("XDCmsBundle:Page")->findOneBy(array('slug' => $slug, 'published' => true));
        
        
        if (!$page) {
            throw $this->createNotFoundException('The page does not exists');
        }
        
        $response = $this->render('XDCmsBundle:Page:show.html.twig', array('page'=> $page));
        
        $response->setSharedMaxAge(40);
        $response->setMaxAge(40);
        
        return $response;        
    }
    
    
    
    
}
