<?php

namespace XD\CmsBundle\Controller;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Symfony\Component\HttpFoundation\RedirectResponse;

use Symfony\Bundle\FrameworkBundle\EventListener\RouterListener;

use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\HttpKernel\EventListener\ResponseListener;

use Symfony\Component\HttpFoundation\Request;

use Sonata\AdminBundle\Controller\CRUDController as Controller;

class PageAdminController extends Controller
{
     public function showAction($id = null) {
    	
    	$page = $this->getDoctrine()->getRepository('XDCmsBundle:Page')->find($id);
    	if($page) {
    		return $response = new RedirectResponse($this->generateUrl('page_show', array('slug' => $page->getSlug())));
    	}
    	
    	throw new NotFoundHttpException('Page does not exist');
    	
    }
}
