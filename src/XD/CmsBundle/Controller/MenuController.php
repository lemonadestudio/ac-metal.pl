<?php

namespace XD\CmsBundle\Controller;

use XD\CmsBundle\Menu\Builder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MenuController extends Controller
{

	/**
	 * Renders managed CMS menu
	 */
    public function managedAction()
    {
        $menu = new Builder();
        $response = $this->render('XDCmsBundle:Menu:managed.html.twig');
        
        $response->setSharedMaxAge(20);
        $response->setMaxAge(20);
               
        return $response;
        
    }

}
