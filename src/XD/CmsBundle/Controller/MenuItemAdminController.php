<?php

namespace XD\CmsBundle\Controller;


use Symfony\Bundle\DoctrineBundle\Registry;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sonata\AdminBundle\Controller\CRUDController as Controller;

class MenuItemAdminController extends Controller
{
	public function saveOrderAction() {
		
		$request = $this->getRequest();
		$parameters = $request->get('elements');
		
		$em = $this->getDoctrine()->getEntityManager();
		
		
		
		$menu_repo = $em->getRepository('XDCmsBundle:MenuItem');
		
		$order = 0;
		if(is_array($parameters)) {
			foreach($parameters as $_id) {
				$menu_item = $menu_repo -> find($_id);
				if($menu_item) {
					$menu_item->setSortOrder($order++);
					
				}
			}
			
			$em->flush();
		}
		
		$response = new Response();
		$response->setContent('ok');
		
		
		
		return $response;
		
	}
}
