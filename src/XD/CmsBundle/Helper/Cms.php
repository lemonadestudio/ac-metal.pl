<?php

namespace XD\CmsBundle\Helper;



class Cms {
	
	public function __construct() {
		
	}
	
	public function make_url($string, $strategy = 'default') {
		
		
		if(!in_array($strategy, array('default'))) {
			$strategy = 'default';
		}
		
		$clean = $string;
		
		switch($strategy) {
			
			case 'default':
				
				$delimiter = '-';
				
				$string = str_replace(
						array('Ą','Ć','Ę','Ł','Ń','Ó','Ś','Ź','Ż','ą','ć','ę','ł','ń','ó','ś','ź','ż'),
						array('a','c','e','l','n','o','s','z','z','a','c','e','l','n','o','s','z','z'),
						$string);
				$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $string);
				$clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
				$clean = strtolower(trim($clean, '-'));
				$clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);
				
				
				break;
				
			default: break;
			
		}
		
		return $clean;
		
		
	}
	
}