Feature: Viewing blog posts
  As a Visitor
  I want to read a blog post

  Background:
    Given I am doing http request on "/"
    
  Scenario: Viewing main page
     When I go to "/"
     Then I should see "Something"

