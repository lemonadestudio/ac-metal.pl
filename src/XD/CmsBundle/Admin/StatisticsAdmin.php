<?php

namespace XD\CmsBundle\Admin;

use Sonata\AdminBundle\Route\RouteCollection;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class StatisticsAdmin extends Admin {

	protected $translationDomain = 'XDCmsBundle';
	

	
	protected function configureRoutes(RouteCollection $collection) {
		
		$collection->remove('create');
		$collection->remove('show');
		$collection->remove('batch');
		$collection->remove('edit');
		$collection->remove('delete');
		
	}

	
	
}