<?php

namespace XD\CmsBundle\Admin;

use Sonata\AdminBundle\Route\RouteCollection;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class ContactFormMessageAdmin extends Admin {

	protected $translationDomain = 'XDCmsBundle';
	
	protected function configureRoutes(RouteCollection $collection) {
	
				$collection->remove('create');
				
				$collection->remove('edit');
// 				$collection->remove('batch');
// 				$collection->remove('show');
// 				$collection->remove('delete');
	
	}
	
    public function configureShowFields(ShowMapper $showMapper) {

        $showMapper
                ->add('senderName')
                ->add('senderEmail')
                ->add('senderMessage')
                ->add('sentAt')
                ;
    }

   
    public function configureListFields(ListMapper $listMapper) {
        
        $listMapper->addIdentifier('id')
        ->add('senderName')
        ->add('senderEmail')
        ->add('senderMessage')
        ->add('sentAt')
                ;
        
    }
    
    public function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
//         $datagridMapper
//             ->add('tag')
            
//         ;
    }
    
    public function configureFormFields(FormMapper $formMapper)
    {
//         $formMapper
            
//              ->add('tag')
              
            
           
//         ;
        

    }
    


}