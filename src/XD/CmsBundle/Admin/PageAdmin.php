<?php

namespace XD\CmsBundle\Admin;


use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class PageAdmin extends Admin {
	
	protected $translationDomain = 'XDCmsBundle';

	
	
	public function getFormTheme() {

		 return array("XDCmsBundle:Form:form_admin_fields.html.twig"); 

	}

	public function getBatchActions()
	{
		$actions = array();
		
		// turn off batch actions
		return $actions;
		
		if ($this->hasRoute('delete') && $this->isGranted('DELETE')) {
			$actions['delete'] = array(
					'label' => $this->trans('action_delete', array(), 'SonataAdminBundle'),
					'ask_confirmation' => true, // by default always true
			);
		}
	
		return $actions;
	}
	
	public function getEditTemplate() {
		
		$template = parent::getEditTemplate();
		// SonataAdminBundle:CRUD:edit.html.twig
		
		return 'XDCmsBundle:CRUD:page_edit.html.twig';
		
	}
	
	
	
    public function configureShowFields(ShowMapper $showMapper) {

        $showMapper
                ->add('title')
                ->add('keywords')
                ->add('description')
                ->add('content');
    }

   
    public function configureListFields(ListMapper $listMapper) {
        
    	
        $listMapper->addIdentifier('title', null, array())
        		->add('description', 'text')
				->add('published', 'boolean')
				->add('onMainPage')
                ->add('image', null, array('template' => 'XDCmsBundle:CRUD:page_list_image.html.twig'))
                ->add('keywords');
        
        $listMapper->add('_action', 'actions', array(
        		$this->trans('actions') => array(
        				'view' => array(),
        				'edit' => array(),
        				'delete' => array()
        		)
        ));
        
//         $listMapper->add('_batch', 'batch', array(
//         		$this->trans('actions') => array(
//         				'view' => array(),
//         				'edit' => array(),
//         				'delete' => array()
//         		)
//         ));

        
        
      
        
    }
    
    public function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
            ->add('content')
            ->add('published')
            ->add('onMainPage')
        ;
    }
    
    public function configureFormFields(FormMapper $formMapper)
    {
    	
        $formMapper
            ->with('General')
                ->add('title', null, array('required' => true))
                ->add('content', null, 
                		array('required' => false, 'attr' => array('class' => 'sonata-medium wysiwyg'))
                		)
                ->add('published', 'checkbox', array('required' => false))
                ->add('publishDate', 'datetime')
                ->add('file', 'file', array('required' => false))
             ->end()
//              ->with('Tags', array('collapsed' => 'true'))
//                 ->add('tags', 'sonata_type_model', array("property_path" => false, 'expanded' => true, 'multiple' => true), array('edit' => 'standard'))
//              ->end()
             
             ->with('SEO', array('collapsed' => false))
             	->add ('automaticSeo', 'checkbox', array('required' => false))
                ->add ('slug', null, array('required' => false))
                ->add('keywords', null, array('required' => false))
                ->add('description', 'textarea', array('required' => false))
            ->end()
            ->with('Main page')
            	->add('onMainPage')
            	->add('onMainPageOrder')
            ->end()
        -> setHelps(array(
                    'title' => $this->trans('Enter page title'),
        			'automaticSeo' => $this->trans('help.automaticseo'),
        			'slug' => $this->trans('help.slug'),
                    'publishDate' => $this->trans('help.publishDate'),
        			'onMainPage' => $this->trans('help.onMainPage'),
        			'onMainPageOrder' => $this->trans('help.onMainPageOrder')
                ));
        

    }
    
    

}