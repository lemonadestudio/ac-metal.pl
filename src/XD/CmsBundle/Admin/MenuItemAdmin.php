<?php

namespace XD\CmsBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class MenuItemAdmin extends Admin {

	protected $translationDomain = 'XDCmsBundle';
	
	protected $datagridValues = array(
			'_page'       => 1,
			'_sort_order' => 'ASC', // sort direction
			'_sort_by' => 'sortOrder' // field name
	);
	
	public function getTypeChoices() {
		
		 return array(
		 		'url' => $this->trans('Manual URL'),
		 		'page' => $this->trans('Internal Page')
		 		);
	}
	
	public function getBatchActions() {
		return array();
		
	}
	
	protected function configureRoutes(RouteCollection $collection) {
		$collection->add('saveorder');
	}
	
	public function getLocationChoices() {
	
		return array(
				'bottom' => $this->trans('Bottom menu'),
		);
	}
	
	
	public function getListTemplate() {
		return 'XDCmsBundle:CRUD:menuitem_list.html.twig';
	}
	
	
	public function getEditTemplate() {
	
		$template = parent::getEditTemplate();
		// SonataAdminBundle:CRUD:edit.html.twig
	
		return 'XDCmsBundle:CRUD:menuitem_edit.html.twig';
	
	}
	
    public function configureShowFields(ShowMapper $showMapper) {

        $showMapper
                ->add('title')
                ;
    }

   
    public function configureListFields(ListMapper $listMapper) {
        
        $listMapper->addIdentifier('title')
        ->add('currentUrl', null, array('template' => 'XDCmsBundle:CRUD:menuitem_currenturl.html.twig'))
        ;
        
    }
    
    public function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
           
            ->add('location', null, array(), 'choice', array('choices' => $this->getLocationChoices()))
        ;
    }
    
    public function configureFormFields(FormMapper $formMapper)
    {
    	
    	
        $formMapper
            ->with('General')
	            ->add('anchor')
	            ->add('title', null, array('required' => false))
	            ->add('location', 'choice', array('choices' => $this->getLocationChoices(), 'expanded' => false))
	            ->add('type', 'choice', array('choices' => $this->getTypeChoices()))
	            ->add('url', 'url')
	            ->add('page', 'sonata_type_model', array('required' => false), array('edit' => 'normal'))
	        ->end()
	        ->with('Advanced')
	        	->add('attributes', 'text', array('required' => false)
	        	)
	        ->end()
              
            ->setHelps(array(
            		'anchor' => $this->trans('help.menuitem.anchor'),
            		'title' => $this->trans('help.menuitem.title'),

            		'attributes' => $this->trans('help.menuitem.attributes')
            		
            		)
           		)
           
        ;
        

    }
    
    
    
    

}