<?php

namespace XD\UserBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\FixtureInterface;
use XD\UserBundle\Entity\User;


class LoadUserData implements FixtureInterface {
    
    /**
     *
     * @param EntityManager $manager 
     */
    function load($manager) {
        
        
        $user1 = new User();
        
        $user1->setUsername('admin');
        
        $user1->setEmail('test@acme.com');

        $user1->addRole('ROLE_SUPER_ADMIN');
        $user1->setPlainPassword('admin');
        $user1->setEnabled(true);
       

        $manager->persist($user1);
        $manager->flush();
        
    }
    
}