<?php
/*
 * This file is part of the Sonata project.
 *
 * (c) Thomas Rabaix <thomas.rabaix@sonata-project.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


namespace XD\UserBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;
use Symfony\Component\Config\Resource\FileResource;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\Config\FileLocator;

use Sonata\UserBundle\DependencyInjection\SonataUserExtension;


/**
 *
 * @author     Thomas Rabaix <thomas.rabaix@sonata-project.org>
 */
class XDUserExtension extends SonataUserExtension
{

    /**
     *
     * @param array            $config    An array of configuration settings
     * @param ContainerBuilder $container A ContainerBuilder instance
     */
    public function load(array $config, ContainerBuilder $container)
    {
        
        parent::load($config, $container);        
        $container->getParameterBag()->set('sonata.user.admin.user.class', 'XD\UserBundle\Admin\Entity\UserAdmin');
        $container->getParameterBag()->set('sonata.user.admin.user.entity', 'XD\UserBundle\Entity\User');
        
        $container->getParameterBag()->set('sonata.user.admin.group.class', 'XD\UserBundle\Admin\Entity\GroupAdmin');
        $container->getParameterBag()->set('sonata.user.admin.group.entity', 'XD\UserBundle\Entity\Group');
      
        
    }

    /*
     *  
        <parameter key="sonata.user.admin.user.class">Sonata\UserBundle\Admin\Entity\UserAdmin</parameter>
        <parameter key="sonata.user.admin.user.controller"></parameter>
        <parameter key="sonata.user.admin.user.entity">Application\Sonata\UserBundle\Entity\User</parameter>
        <parameter key="sonata.user.admin.user.translation_domain">SonataUserBundle</parameter>

        <!-- GROUP -->
        <parameter key="sonata.user.admin.group.class">Sonata\UserBundle\Admin\Entity\GroupAdmin</parameter>
        <parameter key="sonata.user.admin.group.controller"></parameter>
        <parameter key="sonata.user.admin.group.entity">Application\Sonata\UserBundle\Entity\Group</parameter>
        <parameter key="sonata.user.admin.group.translation_domain">%sonata.user.admin.user.translation_domain%</parameter>
     */
 
    public function getAlias()
    {
        return "xd_user";
    }
}