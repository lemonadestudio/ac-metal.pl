<?php

namespace Lm\AcMetalBundle\Controller;


use Symfony\Bundle\FrameworkBundle\EventListener\RouterListener;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Doctrine\ORM\EntityManager;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
	

    /**
     * @Route("/", name="acmetal_main_page")
     * @Route("/{slug}.html", name="acmetal_page_show")
     */
    public function indexAction($slug = '')
    {
       
    	$em = $this->getDoctrine()->getEntityManager();
    	
    	$request = $this->get('request');



    	$pages = $em->createQuery("
    			
    			SELECT p
    			FROM XDCmsBundle:Page p
    			WHERE p.onMainPage = true
    			ORDER BY p.onMainPageOrder ASC, p.title ASC

    			")
    			->execute()
    	;
    	
    	$curr_slug = '';
    	$current_page = null;
    	$all_images = array();
    	$all_images_path = array();
    	$current_image = null;
    	if(is_array($pages)) {
    		$i = 0;
    		foreach($pages as $page) {
    			
    			
    			
    			if($page->getSlug() == $slug or ($slug == '' and $i == 0)) {
    				$curr_slug = $slug;
    				$current_page = $page;
    				$current_image = $page->getWebPath();
    				
    			} 
    			
    			if($i == 0) {
    				$page_route = $this->get('router')->generate('acmetal_main_page');
    			} else {
    				$page_route = $this->get('router')->generate('acmetal_page_show', array('slug' => $page->getSlug()));
    			}
    			
    			if($page->getWebPath()) {
    				$all_images[$page->getUniqid()] = $page->getWebPath();
    			} else {
    				$all_images[$page->getUniqid()] = '';
    			}
    			
	
    			
    			$i++;	
    		}
    	}

    	
    	$response = $this->render('LmAcMetalBundle:Default:index.html.twig', 
    			array(
    					'pages' => $pages, 
    					'all_images' => $all_images,
    					'all_images_json' => json_encode($all_images),
    					'current_image' => $current_image,
    					'slug' => $curr_slug,
    					'current_page' => $current_page
    			));
    	
    	$response->setSharedMaxAge(60);
    	
    	return $response;
    	
    }
    /**
     * @Route("/,ajax", name="acmetal_main_page_ajax")
     * @Route("/{slug}.html,ajax", name="acmetal_page_show_ajax")
     */
    public function pageAjaxAction($slug = '') {
    	
    	$em = $this->getDoctrine()->getEntityManager();
    	 
    	$request = $this->get('request');
    	
    	
    	/*
    	 * ajax get page
    	*/
    	if($request->isXmlHttpRequest()) {
    	
    		if($slug == '') {
    			 
    			$pages = $em->createQuery("
    					SELECT p
    					FROM XDCmsBundle:Page p
    					WHERE p.onMainPage = true
    					ORDER BY p.onMainPageOrder ASC, p.title ASC
    	
    					")->setMaxResults(1)->execute();
    			 
    			if(is_array($pages)) {
    				$page = $pages[0];
    			}
    			//     			$page = $em->getRepository('XDCmsBundle:Page')
    			//     			->findOneBy(
    			//     					array('onMainPage' => true),
    			//     					array('onMainPageOrder', 'ASC'));
    			 
    		} else {
    			 
    			$page = $em->getRepository('XDCmsBundle:Page')->findOneBy(array('slug' => $slug, 'onMainPage' => true));
    			 
    		}
    	
    	
    		$response = $this->render('LmAcMetalBundle:Default:ajax_page.html.twig',
    				array(
    						'page' => $page,
    				));
    		 
    		$response->setSharedMaxAge(60);
    		 
    		return $response;
    	
    	}
    	
    	throw new NotFoundHttpException();
    	
    }
}
