var _changer_interval = null;
var _changer_visible = 0;
var _changer_elements = new Array();
var _changer_stop = false;
var _changer_stop_on = '';


var _dont_push_history = false;
var _is_sliding = false;


$(function(){
	
	mainPageChangerInit();
	
	window.onpopstate = function(event) {  
		if(event.state != null) {

			 if(event.state.main_page_ajax == 'true') {
				 _dont_push_history = true;
				 $('#'+event.state.element_id).click();
			 }
		}
		
	};  
	
	$('.main_page_title > h2 > a').click(function(event){
		
		event.preventDefault();
		if(_is_sliding == true ) {
			return false;
		}
		
		url = $(this).attr('href');
		title = $(this).html();
		el_id = $(this).attr('id');
		
		
		ajax_container = $(this).parents('.main_page_page').find('.main_page_content');
		title_div = $(this).parents('.main_page_title');

		
		if( title_div.hasClass('main_page_title_current')) {
			return false;
			
		}

		
		$('.main_page_title').removeClass('main_page_title_current');
		title_div.addClass('main_page_title_current');
		
		
		if(!ajax_container.hasClass('main_page_ajx_loaded')) {
			
			$.ajax({
				type: 'GET',
				url:  url + ',ajax',
				success: function(ret) {
					ajax_container.find('.main_page_content_inner').html(ret);
					
					_is_sliding = true;
					$('.main_page_ajx_visible').slideUp(1000, function(){
						$('.main_page_content').removeClass('main_page_ajx_visible').addClass('main_page_ajx_hidden');
					});
					
					ajax_container.slideDown(1000, function(){
						ajax_container.addClass('main_page_ajx_loaded').addClass('main_page_ajx_visible').removeClass('main_page_ajx_hidden');
						_is_sliding = false;
					});
				}
				
			});
		} else {
			_is_sliding = true;
			$('.main_page_ajx_visible').slideUp(1000, function(){
				$('.main_page_content').removeClass('main_page_ajx_visible').addClass('main_page_ajx_hidden');
			});
			
			ajax_container.slideDown(1000, function(){
				ajax_container.addClass('main_page_ajx_loaded').addClass('main_page_ajx_visible').removeClass('main_page_ajx_hidden');
				_is_sliding = false;
			});
		}
		
		
		if(title_div.hasClass('main_page_title_first')) {
			_changer_stop = false;
			_changer_stop_on = '';
		} else {
			_changer_stop = true;
			_changer_stop_on = el_id;
		}
		
		console.log(_changer_stop_on);
		

		if(!_dont_push_history) {
			history.pushState({ main_page_ajax: "true", element_id: el_id }, title, url);
		}
		_dont_push_history = false;
		
		
		
		
	});
	
});



function mainPageChangerInit() {
	
	if(_changer_visible != 0) {
		$('.main_page_top_img').removeClass('main_page_top_visible');
		$('.main_page_top_img:first').addClass('main_page_top_visible');
	}
	
	_changer_elements = $('.main_page_top_img');
	if(_changer_elements.length > 1) {
		
		
	    _changer_interval = setInterval(mainPageChanger, 2000);
		
	} else {
		
		clearInterval(_changer_interval);
		
	}

	
}

function mainPageChanger() {
	
	
	if(_changer_stop) {
		
		
		if($("#top-img-"+_changer_stop_on).hasClass('main_page_top_visible')) {
			return;
		}
		
		_changer_elements.removeClass('main_page_top_visible');
		$("#top-img-"+_changer_stop_on).addClass('main_page_top_visible');
		return;
	}
	
	_changer_elements.removeClass('main_page_top_visible');
	
	if(_changer_visible + 1 >= _changer_elements.length ) {
		_changer_visible = 0;
	} else {
		_changer_visible++;
	}
	_changer_elements.eq(_changer_visible).addClass('main_page_top_visible');
	
	
}